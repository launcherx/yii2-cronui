<?php

namespace launcherx\cronui;

use yii\web\AssetBundle;

/**
 * Class CronUIAsset
 *
 * @package launcherx\cronui
 */
class CronUIAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@vendor/launcherx/bootstrap-cronui';

    /**
     * @var array
     */
    public $css = [
        'cronui.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'jquery.cronui.js',
    ];

    /**
     * Set language based on UI language
     */
    public function init()
    {
        parent::init();
        $this->js[] = 'i18n/jquery.cronui-' .  \Yii::$app->language . '.js';
    }

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset'
    ];

}
