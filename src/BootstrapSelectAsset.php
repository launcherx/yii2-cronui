<?php

namespace launcherx\cronui;

use yii\web\AssetBundle;

/**
 * Class BootstrapSelectAsset
 *
 * @package launcherx\cronui
 */
class BootstrapSelectAsset extends AssetBundle
{

    /**
     * @var string
     */
    public $sourcePath = '@npm/bootstrap-select/dist';

    /**
     * @var array
     */
    public $css = [
        'css/bootstrap-select.min.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/bootstrap-select.js',
    ];

    /**
     * Set language based on UI language
     */
    public function init()
    {
        parent::init();
        $this->js[] = 'js/i18n/defaults-' .  str_replace('-', '_', \Yii::$app->language) . '.js';
    }

    public $depends = [
        'yii\web\JqueryAsset'
    ];

}
